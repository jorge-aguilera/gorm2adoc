import grails.core.GrailsApplication
import org.grails.datastore.mapping.model.PersistentEntity
import org.grails.datastore.mapping.model.PersistentProperty
import org.grails.datastore.mapping.model.types.Association
import groovy.text.GStringTemplateEngine

grailsApplication = ctx.getBean('grailsApplication') as GrailsApplication
engine = new GStringTemplateEngine()

Map entityToMap( PersistentEntity pe ){
    Map binding = [
            name : pe.name,

            attributes : pe.persistentProperties.sort { it.name }.inject([], {List list, PersistentProperty pp->
                if ( !(pe.associations*.name).contains(pp.name)) {
                    list.add "$pp.type.simpleName $pp.name"
                }
                list
            }),

            owners : pe.owners?.inject([],{ List list, Class ow->
                list.add "$pe.name <- $ow.name"
                list
            }),

            associations: pe.associations.inject([],{ List list, Association aa->
                int idx = list.size()
                String direction = '<-'

                if ("$aa".startsWith("one-to-many"))
                    direction = " \"1\" *${'-'.multiply(idx+1)} \"many\""

                if ("$aa".startsWith("many-to-many"))
                    direction = " \"many\"  *${'-'.multiply(idx+1)}* \"many\" "

                if ("$aa".startsWith("many-to-one"))
                    direction = " \"many\" *${'-'.multiply(idx+1)} \"1\" "

                list.add "$pe.name $direction $aa.associatedEntity.name : $aa.name"
                list
            }),
    ]
}

String entity2PlantUML( PersistentEntity pe ) {

    String template = '''
class $name{
${attributes.join('\\n')}
}
${associations.join('\\n')}
'''

    Map binding = entityToMap(pe)
    engine.createTemplate(template).make(binding).toString()
}

void dumpEntity( PersistentEntity pe ){
    File f = new File("src/docs/asciidoc/details/${pe.name}.adoc")
    f.parentFile.mkdirs()
    f.text = "[plantuml]"
    f << '\n----\n'
    f << entity2PlantUML(pe)
    List dumped = [pe.name]
    pe.associations.eachWithIndex{ Association entry, int i ->
        if( dumped.contains(entry.associatedEntity.name))
            return
        f << entity2PlantUML(entry.associatedEntity)
        dumped.add entry.associatedEntity.name
    }
    f << "\n----\n"
}


void dumpEntitties() {
    grailsApplication.mappingContext.persistentEntities.each { PersistentEntity pe ->
        println "generating $pe.name"
        dumpEntity(pe)
    }
}

void dumpIndex() {
    File gormAdoc = new File("src/docs/asciidoc/gorm.adoc")
    gormAdoc.text = """
= Domain Objects

"""
    grailsApplication.mappingContext.persistentEntities.sort { it.name }.each { PersistentEntity pe ->
        gormAdoc << "== $pe.name \n"
        gormAdoc << "include::src/docs/asciidoc/details/${pe.name}.adoc[]\n"
        gormAdoc << "<<<<\n"
    }
}


dumpEntitties()

dumpIndex()
